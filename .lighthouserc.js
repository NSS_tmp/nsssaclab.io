module.exports = {
    ci: {
        collect: {
            settings: {chromeFlags: '--no-sandbox'},
            staticDistDir: './',
        },
        assert: {
            preset: 'lighthouse:no-pwa',
        },
        upload: {
            target: 'temporary-public-storage',
        },
    },
};