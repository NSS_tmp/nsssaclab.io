package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	path2 "path"
	"path/filepath"
	"strings"
	"sync"
	"text/template"
	"time"
)

// go run utils/obj.go --path=nsssac/content/clubs --url=http://gitlab.com/nsssac/data/-/raw/master/clubs.json --id=club --json-id=clubs

var name string
var path string
var url string
var noShow bool
var id string
var extraArgs string
var httpTimeout time.Duration

// TODO: cleanup by actually having a struct for the JSON

type Data struct {
	Name        string
	ObjectsName string
	NoShow      bool
	Date        string
	ID          string
	SubID       string
	URL         string
	ExtraArgs   string
	Types       []interface{}
	Presidents  []interface{}
	Execs       []interface{}
	Staff       []interface{}
}

const tmplSrc = `+++
title = "{{ .Name }}"
date = "{{ .Date }}"
draft = false
tags = ["{{ .ObjectsName }}", "object"{{ range .Types }}, "{{ . }}" {{ end }}{{ if .NoShow }}, "no-show"{{ end }}]
authors = ["bot"]
presidents = [""{{ range .Presidents }}, "{{ . }}" {{ end }}]
execs = [""{{ range .Execs }}, "{{ . }}" {{ end }}]
staff = [""{{ range .Staff }}, "{{ . }}" {{ end }}]
+++

{{"{{"}}< obj id="{{ .ID }}" sub_id="{{ .SubID }}" url="{{ .URL }}" {{ .ExtraArgs }} >{{"}}"}}
`

func main() {
	flag.StringVar(&name, "name", "", "name in Markdown files")
	flag.StringVar(&path, "path", ".", "destination path of Markdown files")
	flag.StringVar(&url, "url", "", "URL of JSON file")
	flag.StringVar(&id, "id", "", "ID in Markdown files")
	flag.StringVar(&extraArgs, "extra-args", "", "extra arguments for obj shortcode")
	flag.BoolVar(&noShow, "no-show", false, "set no-show tag")
	flag.DurationVar(&httpTimeout, "timeout", 5*time.Second, "timeout for HTTP requests (overridden by NSSSAC_HTTP_TIMEOUT)")
	flag.Parse()
	if url == "" {
		log.Fatalln("url must not be blank")
	}

	// Get data
	client := &http.Client{Timeout: httpTimeout}
	resp, err := client.Get(url)
	if err != nil {
		log.Fatalf("HTTP request: %s", err)
	}
	defer resp.Body.Close()

	// Unmarshal data
	data := map[string]map[string]map[string]interface{}{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		log.Fatalf("JSON unmarshal: %s", err)
	}

	var entries map[string]map[string]interface{}
	var ok bool
	if entries, ok = data[id]; !ok {
		log.Fatalf("ID not present")
	}

	tmpl, err := template.New("root").Parse(tmplSrc)
	if err != nil {
		log.Fatalf("template parse: %s", err)
	}

	var wg sync.WaitGroup

	// Make Markdown files
	for key, entry := range entries {
		wg.Add(1)
		key := key
		entry := entry
		go func() {
			defer wg.Done()
			p, err := filepath.Abs(path2.Join(path, fmt.Sprintf("%s-%s.md", id, key)))
			if err != nil {
				log.Printf("%s path absify: %s", key, err)
			}
			f, err := os.Create(p)
			if err != nil && !os.IsNotExist(err) {
				log.Printf("%s file create: %s", key, err)
				return
			}
			defer f.Close()
			name_, ok := entry["name"]
			if !ok {
				name_ = ""
			}

			types := []interface{}{}
			types_, ok := entry["types"]
			if ok {
				types, ok = types_.([]interface{})
				if !ok {
					types = []interface{}{}
				}
			}

			staff := []interface{}{}
			staff_, ok := entry["staff"]
			if ok {
				staff, ok = staff_.([]interface{})
				if !ok {
					staff = []interface{}{}
				}
			}

			presidents := []interface{}{}
			presidents_, ok := entry["presidents"]
			if ok {
				presidents, ok = presidents_.([]interface{})
				if !ok {
					presidents = []interface{}{}
				}
			}

			execs := []interface{}{}
			execs_, ok := entry["execs"]
			if ok {
				execs, ok = execs_.([]interface{})
				if !ok {
					execs = []interface{}{}
				}
			}

			date := "1970-01-01T00:00:00"
			time_, ok := entry["time"]
			if ok {
				date = time_.(string)
			}
			date_, ok := entry["date"]
			if ok {
				date = date_.(string)
			}

			err = tmpl.Execute(f, Data{
				Name:        name_.(string),
				ObjectsName: strings.ToLower(name),
				Date:        date,
				ID:          id,
				SubID:       key,
				URL:         url,
				NoShow:      noShow,
				ExtraArgs:   extraArgs,
				Types:       types,
				Presidents:  presidents,
				Execs:       execs,
				Staff:       staff,
			})
			if err != nil {
				log.Printf("%s template execute: %s", key, err)
				return
			}
			f.Close()
		}()
	}
	wg.Wait()
}
