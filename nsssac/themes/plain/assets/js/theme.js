
const styleMap = {
    "light": ["#04b", "#fff", "#eee"],
    "dark": ["#0cf", "#000", "#333"],
}

function getThemeLayout() {
    let themeSetting = localStorage.getItem("theme");
    let layoutSetting = localStorage.getItem("layout");
    if (themeSetting === "custom") {
        themeSetting = "light"; // custom isnt working too good, so fallback
    }
    layoutSetting = "vertical";
    if (themeSetting !== "dark" && themeSetting !== "light") {
        console.warn(`getThemeLayout was called with invalid theme ${themeSetting}, defaulting`)
        themeSetting = "light"; // invalid, so default
    }
    return [themeSetting, layoutSetting]
}

function setThemeLayout(name, layout) {
    if (layout === null) {
        layout = getThemeLayout()[1];
    }
    if (name === null) {
        name = getThemeLayout()[0];
    }
    if (name === "custom") {
        return;
    }
    let root = document.documentElement;
    root.style.setProperty("--accent-fg", styleMap[name][0]);
    root.style.setProperty("--accent-bg", styleMap[name][1]);
    root.style.setProperty("--accent-plain", styleMap[name][2]);
    // abandon layout for now
    localStorage.setItem("theme", name);
    localStorage.setItem("layout", layout);
}

function loadThemeLayout() {
    let settings = getThemeLayout();
    setThemeLayout(settings[0], settings[1]);
}

loadThemeLayout();
