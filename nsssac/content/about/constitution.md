+++
title = "Constitution"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata"]
+++

# The Constitution of the Northern Secondary School Student Affairs Council

## Chapter One: General Matters

#### Article I: Name
1. The Organization will be known as the Northern Secondary School Student Affairs Council (NSS SAC), and is referred to hereafter in this document as the Student Affairs Council (SAC).

#### Article II: Purpose
1. The purpose of the SAC is to:
   1.1. Promote the general welfare of the Student Body;
   1.2. Represent the Student Body in community interests;
   1.3. Represent the Student Body in all levels of the school;
   1.4. Actively seek student input on all issues regarding student welfare;
   1.5. Promote a positive relationship and liaise between the Student Body and other parties, including, but not limited to the Administration and the Toronto District School Board; and
   1.6. Provide a degree of social functions.
   
#### Article III: Powers
   1.The SAC, as a collective, is empowered to the following:
1. Act on decisions made by the Executive, Senate and the House of Representatives;
2. Determine the amount of the Student Activities Fee in consultation with the Administration;
3. Administer the Student Activities Fee;
4. Represent the Student Body on all student committees, all staff-student committees, and all committees outside the school relating to, and concerned with the students of Northern Secondary School; and
5. Sanction student organizations such as clubs and associations as well as other extracurricular activities.

### Article IV: Membership
2. The SAC will consist of three levels of student government, namely: 2.1. The Executive,
   2.2. The Senate, and
   2.3. The House of Representatives,
   2.4. In addition to the Speaker and Deputy Speaker.
3. The Executive, the top-most level of government within the SAC, consists of the following positions:
   3.1. The President,
   3.2. The Vice-President,
   3.3. The Treasurer,
   3.4. The External Social Director, 3.5. The Internal Social Director, and 3.6. The Secretary.
4. The Senate shall consist of the following members:
   Note: Those who possess voting rights are marked with an asterisk (*); Appointed positions are marked with a cross (+). Refer to Article IX for more details regarding Appointed Positions.
   4.1. The Speaker+ and Deputy Speaker+,
   4.2. The Vice-President, who shall act as Chair,
   4.3. The remainder of the Executive members, each with
   one vote,
   4.4.The Grade Senators*+ of each Grade,
   4.5. Two TDSB Super Council Senators*+,
   4.6. The Northern Foundation Senator*+,
   4.7. The PTA Senator*+,
   4.8. The Webmaster Senator*+,
   4.9. The Audio/Visual Senator*+,
   4.10. The Advertising Senator*+,
   4.12. The Safe School Senator*+,
   4.13. One senator from each association, approved by the Executive*.
5. The House of Representatives, also known as the General Assembly, consists of the following members:
   5.1. The Speaker and Deputy Speaker,
   5.2. The Executive,
   5.3. One Representative from each homeform,
   5.4. One Vice-Representative from each homeform.
6. All Speakers, members of the House of Representatives, Senators, and Executives must be full time students at Northern Secondary School.

## Chapter Two: Duties

### Article V: Duties and Responsibilities of the Executive
7. The duties of all members of the Executive shall include the following responsibilities:
   7.1. Initiate bills of policy and action as they relate to the general welfare of the Student Body;
   7.2. Initiate committees whenever necessary;
   7.3. With due case, initiate a Bill of Impeachment of any member of the House of Representatives or Senate;
   7.4. Determine and administer a specific amount of money annually for the purpose of student scholarships;
   7.5. Select its own staff advisor(s) each year from a group of qualified and willing candidates;
   7.6. Act as a liaison between students and staff;
   7.7. Openly seek input from students on policy and SAC events;
   7.8. Present and reflect the interests of the Student Body;
   7.9. Advise and assist each other;
   7.10. Meet regularly with the constituency directly;
   7.11. Provide contact details, official and/or personal for easy access by the Student Body by means of homeform classroom posts and any other effective means;
   7.12. Meet with each other no less than once a week. Full quorum, consisting of all six Executives, is necessary; and,
   7.13. Attend all Executive, Senate, and House of Representative meetings.
   7.14. Every member must have a vested interests in the success of the SAC as a whole, and individual actions must work to attain this goal.
   7.15. Each member must fully read the Constitution and other effectual legislation prior the beginning of their term of office in September. He/she is to ensure that the most updated copy of the
   Constitution and other effectual legislation is available at each House of Representatives, Senate, and Executive meeting.
   7.16. Responsibilities of Executive positions take precedence over all other extracurricular activities. Members of the Executive may participate in other co-curricular activities only if they make a case to the rest of the Executive, which will decide on whether such additional commitments will hinder their ability to conduct their SAC duties.
8. The following is an outline of the duties of Executive members, as they apply to each specific position:
   8.1. The President is responsible for the following obligations:
   8.1.a. Ensure the co-operation and smooth operation of the Executive as a team;
   8.1.b. Ensure that all business passed by the House of Representatives is followed through and carried out;
   8.1.c. Call all meetings of the House of Representatives and all meetings of the Executive;
   8.1.d. Keep the Staff Advisor(s) informed of all SAC activities on a weekly basis; 8.1.e. Sanction all SAC committees other than the social ones;, 8.1.f. Designate SAC representation on committees throughout the school;
   8.1.g. Ensure representation of the SAC on the TDSB SuperCouncil, and School Council;
   8.1.h. Administer the SAC budget, as outlined in the Budget Act, in conjunction with the Treasurer; and
   8.1.i. Represent or designate representation for all major school functions, including Commencement.
   8.2. The Vice-President is responsible for the following obligations: 8.2.a. Act for the President in their absence;
   8.2.b. Assume the Presidency in the event that the office becomes vacant; 8.2.c. Act as the first designate of the President;
   8.2.d. Chair the Senate, and find an Executive replacement to take over in their absence at a Senate meeting;
   8.2.e. Be responsible for the formation of the Senate by appointing any necessary vacancies and positions, in consultation with the other Executive members. All appointed members must be approved by the Grade Senators and House of Representatives;
   8.2.f. Maintain communication between the Senate and the Executive;
   8.2.g. Chair the Elections Committee and preside over all elections concerning the SAC; 8.2.h. Oversee the formation and operation of all organizations represented in the Senate; and
   8.2.i. Counsel and support the President.
   8.3. The Treasurer shall have the following responsibilities
   8.3.a. Act as the second designate to the President;
   8.3.b. Be responsible, with consent, review, and supervision from the Staff Advisor(s) and/or administration staff designate(s) for all the financial aspects of the SAC monies including asset control, and all monies generated from all SAC funded clubs, associations, and activities;
   8.3.c. Be responsible for all preparation leading to the establishment of a budget as outlined in the Budget Act;
   8.3.d. Keep an accurate and updated record of all SAC assets;
   8.3.e. Be responsible for auditing all SAC sanctioned clubs and associations, as outlined in the Budget Act;
   8.3.f. Distribute a financial statement regularly to all House of Representatives members; and
   8.3.g. Prepare a final financial statement for the year, as outlined in the Budget Act. 8.4. The External Social Director shall have the following responsibilities;
   8.4.a. Ensure, with the collaboration of the Treasurer, all proceeds from external social events are deposited in the SAC account;
   8.4.b. Act as assistant to the Internal Social Director in all internal social activities;
   8.4.c. Research, plan, and implement all out-of-school social activities, including but not limited to formals, and semi-formals;
   8.4.d. Develop alternative and/or additional out-of-school event(s) if and when the need or desire for such external events arise; and
   8.4.e. Keep the Staff Advisor(s), Executive, Senate, and House of Representatives informed on the progress of external social activities.
   8.4.f. Be responsible for the design, organization, and distribution of Northern Apparel.
   8.5. The Internal Social Director shall have the following responsibilities: 8.5.a. Act as an assistant to the External Social Director in all external social events;
   8.5.b. Ensure, with the collaboration of the Treasurer, that all proceeds from the internal social events are deposited in the SAC account;
   8.5.c. Research, plan, and implement all in-school social activities;
   8.5.d. Preside over monthly meetings with chartered club and association presidents to update each other on school events and SAC initiatives;
   8.5.e. Approve, in consultation with the Executive, all charters of extra-curricular organizations, and revoke those charters when deemed appropriate.
   8.6. The Secretary shall have the following responsibilities
   8.6.a. Make available a copy of the Constitution, Budget Act, Elections Act and all other effectual legislation to all members of the Senate and House of
   Representatives electronically.
   8.6.b. Be responsible, with the help of the Speaker and Deputy Speaker, for all preparation and archiving of Bills both physically and electronically;
   8.6.c. Be responsible, with the help of the Speaker and Deputy Speaker, for the recording of
   all voting results;
   8.6.d. Prepare and distribute an agenda one day prior to, and minutes following all House of Representatives, Senate, and Executive meetings, making it
   accessible to all those to whom it pertains;
   8.6.e. Be responsible for the distribution of agendas and minutes to each member of the House of Representatives;
   8.6.f. Conduct or delegate the taking of attendance at all House of Representatives meetings;
   8.6.g. Ensure the taking of attendance at Senate meetings;
   8.6.h. Be responsible for all correspondences concerning the SAC, including, but not limited to monitoring the SAC mailbox, e-mail, and voice mail;
   8.6.i. Be responsible for all written documents presented by the SAC; 8.6.j. Maintain the files of Executive members;
   8.6.k. Maintains contact details of people with which the SAC conducts business; 8.6.l. Keep copies of activity calendars and special events;
   8.6.m. Write notes of courtesy, thank you, congratulations, and the like; and

### Article VI: Duties of the Senate
9. The duties of the Senate as a general body are outlined as follows:
   9.1. Act as an advisory body to the Executive;
   9.2. Advise the Executive on behalf of, and in the best interest of the group each Senator represents;
   9.3. Vote on all Bills proposed by the Executive and House of Representatives members before they are presented for approval before the House of
   Representatives;
   9.4. Assume particular responsibilities of the Executive at the request of a two-thirds majority of the House of Representatives. Note all references to a majority in the constitution signify the majority of only the voting population;
   9.5. Meet with the constituency directly;
   9.6. Provide contact details, official and/or personal for easy access by the Student Body;
   9.7. Meet with each other no less than once every two weeks; and
   9.8. Meet no less than once every month with the entire Executive, excluding House of Representatives meetings. The Executive has the right to flex timelines if and when deemed necessary.
   9.9. The Senate is the sole​ body able to initiate a Bill of Impeachment naming a member of the Executive.
   9.10. The Senate has the power to initiate any Bills in the House of Representatives except a Bill of Request demanding something that could have been done by the individual issuing the Bill himself/ herself.
   9.11. The particular duties of each Senator are outlined as follows:
   9.11.a. Attend all Senate meetings;
   9.11.b. Represent the concerns of their particular grade or organization in the House of Representatives; and
   9.11.c. Participate in all major SAC activities and events. Failure to participate in three SAC sanctioned events will lead to a Bill of Impeachment being
   forwarded against a Senator.
   9.12. If a Senator misses three consecutive meetings, or four non consecutive meetings without valid completion of an excusal form, the senator will be immediately expelled from the Senate. Senate duties should take priority over other school related activities and is not a valid reason to miss Senate meetings.
   9.13. A Bill of Impeachment will be put forward by the Speaker, in conjunction with the Executive if a Senator is demonstrating behavior which is deemed
   unconstitutional or impeding the Senate’s process and/or duties.
   
### Article VII: Duties of House of Representatives
1. The Class Representative of each class is responsible for the following duties: 1.1. Attend all House of Representatives meetings;
   1.2. Read and post the minutes from House of Representatives meetings, in addition to all financial statements in their homeform at the first opportunity;
   1.3. Raise concerns of their constituents to the Grade Senator;
   1.4. Organize a class referendum on any issue at the request of any class member, and report the results of this referendum at an House of Representatives meeting or to the President;
   1.5. Conduct any referendums sanctioned by the Executives and report the results or any such referendums to the House of Representatives or to the President;
   1.6. Be responsible for administering the class United Way project with the Vice-Representative;
   1.7. Be responsible for homeform recycling;
   1.8. Meet with their Grade Senator no less than once a month;
   1.9. Relinquish their position to their Vice-Representative upon being elected Grade Senator, or appointed Senator; and
   1.10. Approve any appointments made by the Vice-President and Executive that has already been passed in the Senate.
2. The Vice-Representative of each class is responsible for the following duties: 2.1. Assume the responsibilities of the Class Representative only in their absence.
   2.2. The Vice-Representative is allowed to run for any position on the Senate excluding the position of Grade Senator. 
   
### Article VIII: Duties of the Speaker and Deputy Speaker
1. The duties of the Speaker shall include the following responsibilities: 1.1. Oversee the provisions of the Constitution and other effectual legislation;
   1.2. Open the House of Representatives meetings each year with a speech to the House of Representatives explaining the essence of the Constitution and the
   responsibilities of each body of the SAC and the members therein;
   1.3. Chair all House of Representatives meetings and be responsible for maintaining parliamentary procedure;
   1.4. Reserve the right of expulsion, with due cause, of any House of Representatives member;
   1.5. Note all deviations from the effectual legislation, which occur in the course of a House of Representatives ​meeting. Furthermore, outline the consequences and options of order open to the House of Representatives as outlined by the effectual legislation;
   1.6. Inform the Senate if, at any time, the Executive, or members therein, are deviating from the effectual legislation in regard to the SAC;
   1.7. Officiate all House of Representative voting and determine whether votes are to be taken on an open or closed ballot;
   1.8. Sign all enacted bills and provide a copy to the Secretary for attachment to the proper document(s);
   1.9. Interpret the Constitution where there is any ambiguity and record all decisions as precedents. Precedents are to be followed unless circumstances have altered so as to render the precedent obsolete; and
   1.10. Attend all Senate and House of Representatives meetings.
   1.11. Enforce the Constitution in conjunction with the deputy when witnessing it broken at any level of government
   1.12. Appropriately appoint a Deputy Speaker in consultation with the Executive in time for the first House of Representatives meeting, approved by the Staff Advisor.
2. The duties of the Deputy Speaker shall include the following responsibilities: 2.1. Attend all House of Representative and Senate meetings;
   2.2. Act as assistant to the Speaker, and take on all duties of the Speaker in their absence; and
   2.3. Record all motions, including the name of the member making the motion, and the results of the voting. The Deputy speaker shall provide a copy of the records to the Secretary.
3. The Speaker shall be designated the year prior by the incoming Executive in  conjunction with the Executive of the prior year.
   
### Article IX: Appointed Positions
1. All appointed positions are governed by the following regulations:
   1.1. All appointed members must be selected by the Vice-President;
   1.2. The interviews for these positions must be held by the Vice-President and at least one other Executive;
   1.3. These appointments are to be completed at least two weeks prior to the first Senate meeting or House of Representatives meeting, whichever one is first, in the September of the new academic year; and
   1.4. The appointments must be agreed upon by all other members of the Executive.
2. New appointed positions that would be helpful to the running of events and activities throughout the year can be developed from time to time, and follow the same selection procedure as per Article IX: Appointed Positions, Section 1.
3. This section describes the various appointed positions in the Senate:
   3.1. The Speaker and Deputy Speaker positions are described as follows:
   3.1.a. One Grade Eleven or Twelve student shall be Speaker and one Grade Ten student shall be Deputy Speaker;
   3.1.b. They are to preside over all House of Representatives meetings and Senate meetings;
   3.1.c. They are to be appointed by the incoming Executive in September of the new academic year, and at least two weeks prior to the first Senate or House of Representatives meeting, whichever one is first;
   3.1.d. The candidates for Speaker and Deputy Speaker shall undergo an interview process with the Vice-President before appointment;
   3.2. The TDSB SuperCouncil Senators are described as follows:
   3.2.a. The TDSB SuperCouncil Senators must represent the interests of the students of
   Northern Secondary School in the TDSB SuperCouncil, and liaise between the SuperCouncil and the Senate.
   3.2.b. The TDSB SuperCouncil Senators must attend all TDSB SuperCouncil meetings.
   3.2.c. The TDSB SuperCouncil Senator must abide to the total responsibilities and guidelines that apply to all other Senators.
   3.3. The NSS Foundation Senator, whose selection process is different from that of other appointed positions is described as follows:
   3.3.a. The NSS Foundation Senator must have a vested interest in preserving the history of Northern Secondary School, as well as promoting its further development through the NSS Foundation.
   3.3.b. The NSS Foundation Senator must attend all NSS Foundation meetings.
   3.3.c. The NSS Foundation Senator undergoes the same selection process as that of appointed position.
   3.3.d. If the NSS Foundation does not approve of the appointed Senator, they may request the Executive to put forward a Bill of Impeachment.
   3.3.e. Apart from this difference, the NSS Foundation Senator must abide to the total responsibilities and guidelines that apply to all other Senators.
   3.4. The PTA Senator is described as follows:
   3.4.a. The PTA Senator must represent the Student Body in the School Council, and liaise between the School Council and the Senate.
   3.4.b. The PTA Senator must attend all School Council meetings.
   3.4.c. The PTA Senator must abide to the total responsibilities and guidelines that apply to all other Senators.
   3.5. The Webmaster Senator is described as follows:
   3.5.a. The Webmaster Senator must perform the following duties:
   3.5.a.i. Work closely with the Executive, officially communicating in person and/or in writing with the Secretary at least twice each week, and with the President at least once a week;
   3.5.a.ii. Be responsible for the timely maintenance of all SAC-relevant domain, hosting, and CMS services, e.g., purchases, renewals, migrations, etc.;
   3.5.a.iii. Upon the request of the Secretary, upload all enacted legislation, Senate and Assembly minutes and agendas within thirty-six hours of reception;
   3.5.a.iv. Generate and/or integrate web developments as soon as they have been approved by the Executive, e.g., creation of Internet polls, expansion of Web presences, incorporation of new technologies, etc.;
   3.5.a.v. Report monthly and develop frequently techniques aimed at increasing Web traffic, e.g., use of Google Analytics or similar software; and
   3.5.a.vi. Monitor and report to the Executive, Senate, and Assembly all other pertinent Web presences concerning the Student Affairs Council, e.g., Social-networking groups, forums, etc.
   3.5.a.vii. Act as assistant to the Secretary if and when requested by the Executive. 3.5.b. The Webmaster is governed by the following additional points: 3.5.b.i. The Webmaster must only perform activities approved by the Executive.
   3.5.b.ii. Other members of the SAC - including Executives and Senators – are permitted to operate certain SAC Web presences as long as they have the ability to do so (i.e., Grade Senators may be moderators of their own Forum Categories, or Secretaries may post agendas on the official Website by themselves). However, the Webmaster is never to be exempt from the duties outlined in Article IX, Section 3.5, Subsection b.ii. If ever the tasks fail to be performed, even if someone else had been conducting it initially, the Webmaster is to take consequence by default.
   3.5.c. During the interview, samples of work are highly encouraged.
   3.5.d. The Webmaster Senator must abide to the total responsibilities and guidelines that apply to all other Senators.
   3.6. The Advertising Senator is described as follows::
   3.6.a. The Advertising Senator must be creatively responsible of all advertisement done by the SAC.
   3.6.b. During the interview, samples of work are highly encouraged.
   3.7. The Advertising Senator must abide to the total responsibilities and guidelines that apply to all other Senators.
   3.8. The Audio/Visual Senator is described as follows:
   3.8.a. The Audio/Visual Senator is responsible for the creation, filming, and editing of all SAC video and audio material.
   3.8.b. The Audio/Visual Senator must abide to the total responsibilities and guidelines that apply to all other Senators.
   3.10. The Safe Schools Senator is described as follows:
   3.10.a. The Safe Schools Senator must attend all meetings of the House of Representatives to respond to questions and concerns brought forward by class representatives.
   3.10.b. The Safe Schools Senator must liaise between the House of Representatives and the Northern administration on matters concerning student safety,
   comfort,
   and infrastructure repair or additions.
   3.10.c. The Safe Schools Senator must attend each quarterly Safe Schools meeting to discuss the following topics concerning the safety of the Student Body:
   3.10.c.i. The condition of all Northern student restrooms.
   3.10.c.ii. The condition of other student frequented areas of the building.
   3.10.c.iii. Any concerns from the student body brought forth by the Executive or House of Representatives relating to student safety.
   3.10.d. The Safe Schools Senator must regularly inspect the following areas of the building and bring any infrastructure concerns to the administration
   immediately:
   3.10.d.i. All Northern student restrooms.
   3.10.d.ii. All water fountains
   3.10.d.iii. Any other areas requiring immediate attention excluding Northern classrooms.
   3.10.e. The Safe School Senator must abide to the total responsibilities and guidelines that apply to all other Senators.

### Article X: Staff Advisors
1. The role of the Staff Advisor(s) shall be to advise the SAC and to represent the staff in regards to the
   SAC.
2. The Staff Advisor(s) shall attend all House of Representatives meetings, and shall attend Executive and
   Senate meetings when possible. In their absence, a replacement adult figure should be present at the Senate meeting in order for the meeting to occur. Failing proper and adequate supervision, the Staff Advisor will have the right to cancel said meetings.
3. It should be noted that SAC is the students’ council. Advisors are to serve in an advisory capacity. They should not control discussion at meetings. Staff Advisors should advise and give points of information, but not attempt to sway opinions by using their influential positions.
4. The Staff Advisor(s) should teach parliamentary procedure, leadership skills, responsibility and any other qualities they think necessary for a good member to have. They should support the SAC’s final resolution. They should offer their wisdom and counsel to help plan projects.
5. In order to remove (a) Staff Advisor(s), a SAC member must obtain the signatures of the majority of the Executive, and have the written concerns presented to the Administration, which will then give its approval.
6. The Staff Advisor has the authority to sanction and/or remove any member of the Executive, Senate, or House of Representatives if their behaviour is deemed contrary to proper leadership and representation as a member of the SAC.
   
## Chapter Three: Procedures

### Article XI: Meetings
1. The following are general procedures governing official meetings held by the SAC:
   1.1. Meetings shall be for the purpose of dealing with business under the jurisdiction of the SAC, of which include the following:
   1.1.a. Discussion and debate of SAC policies and actions; and
   1.1.b. The proposal, amendment, and voting of all Bills.
2. The following are regulations governing all Executive meetings:
   2.1. The Executive shall meet no less than once a week.
   2.2. Quorum at an Executive meeting shall consist of no less than one hundred percent of the Executive. No meeting shall be valid without full quorum.
   2.3. Any Executive member who is absent for more than four meetings in the school year without due cause shall have an automatic bill of impeachment placed before
   him/her. This automatic bill of impeachment will be written by the Staff advisor and presented to a Senate member to bring forward.
3. The following are regulations governing all Senate meetings:
   3.1. The frequency of Senate meetings is outlined in Article VI: Duties of the Senate, section 1.5 and 1.6.
   3.2. Quorum at Senate meetings shall consist of no less than half the members of the Senate.
   3.3. Meetings shall be open to observation, at the discretion of the Chair.
4. The following are regulations governing all House of Representatives meetings:
   4.1. The purpose of keeping Class Representatives informed on policies and actions independently undertaken by the Executive and Senate.
   4.2. All House of Representatives meetings shall:
   4.2.a. Follow Canadian Parliamentary procedure;
   4.2.b. Be held in an open environment; and
   4.2.c. Be chaired by the Speaker, who shall retain the right of expulsion.
   4.3. Quorum at House of Representatives meetings shall consist of fifty percent of the House of Representatives.
   4.4. Each of the Speaker, Executive, the Senate, and House of Representatives and observers shall sit separately at all meetings; and
   4.5. Each member of the House of Representatives shall sit in a predetermined seat which indicates the grade they represent. The Speaker reserves the right to assign and change seating as deemed necessary.
   4.6. Observers or visitors who are not members of the SAC may speak at meetings only with the approval of the Speaker prior to the meeting. Guests will have the same privileges as members, other than to vote or table motions.
   4.7. Each House of Representatives meeting must have time allocated for a General Question Period, during which members of the Senate and House of Representatives may ask questions to the Executive; and
   4.8. Each House of Representatives meeting must have time allocated for the purpose of voting on Bills. All bills shall have been presented to the Executive and Speaker at least one week prior to presentation in the Senate and shall have been presented to, and voted on at a Senate meeting first.
   4.9. The Principal or designate of the Administration shall be informed of the occurrence of all House of Representatives meetings.
   4.10. No more than twenty days shall be elapsed between meetings of the House of Representatives, and no less than ten meetings of the House of Representatives must occur during the course of the school year. The Executive has the right to flex timelines if deemed necessary.
   4.11. Meetings shall be called by the Executive. If more than twenty days elapse between meetings, the Senate may call a meeting.

### Article XII: Bills and Amendments
1. All actions and policies of the SAC shall be passed by Bills in the Senate, then House of Representatives.
2. All bills need to be in written form when presented to the Senate for first discussion and approval.
3. Bills may be proposed by any Executive, House of Representatives or Senate member. Bills should be presented to the Executive and Speaker at least one week prior to submission in the Senate, for examination. All bills need to be approved by the Senate prior to presentation in the House of Representatives.
4. A short debate shall precede all votes on Bills.
5. Amendments may be made to the Bill prior to voting; and
6. Amendments proposed by the Executive must have Senate approval.
7. A simple majority (fifty percent plus one) is required to pass Bills, unless otherwise stated.
8. The Executive retains its right to veto any Bill, at which point it returns to the House of Representatives.​ A second vote with two-third majority (sixty-seven percent) supersedes an Executive veto.
9. Bills of Impeachment, Constitutional Amendments, or Bills of Request requires a three quarter majority (seventy-five percent) to pass.
10. A defeated Bill may be brought back to the House of Representatives after necessary amendments, after ​re-approval by the Senate. A Bill that has not been amended may not be reintroduced before the House of Representatives until a later date.
11. All Bills shall be recorded by the Secretary. This record shall include the name of the Bill, the date of its proposed, the proposing body and member, any amendments, the
    amending body member, and the result of the voting. Records shall remain in the permanent files of the SAC.
12. All Bills must be signed by the Speaker and Staff advisor before they become legal. The Speaker and Staff Adviser shall refuse to sign any Bill which is deemed to be unconstitutional or conceived through unconstitutional means.

## Chapter Four: Relevant Groups

### Article XIII: Internal Committees
1. There shall be two types of internal committees:
   1.a. Principal Committees: Committees continued on a permanent basis; and 1.b. Secondary Committees: Committees created to perform a specific task.
2. The SAC Committees can be initiated by any member of the House of Representatives and shall be sanctioned by the Senate.
3. The chairperson of each SAC Committee shall be appointed by the Executive. Each chairperson must be from the Senate or Executive, and such an appointment is determined by the President, Vice-President, and Treasurer; and ratified by the Senate, then House of Representatives.
4. The chairperson of each Internal SAC Committee shall be responsible for, and shall report on all actions and results of their committee to the Senate, and if deemed necessary, to the House of Representatives.

### Article XIV: External Committees
1. Membership on SAC Committees outside the school shall come from anywhere in the student body, except where noted, with approval of the Executive.
2. Members on these committees last for the duration of the school year, or as long as the committee continues to operate.
3. The chairperson of each External SAC Committee shall be responsible for, and shall report on all actions and results of their committee to the Senate, and if deemed necessary, to the House of Representatives.

### Article XV: Clubs and Associations
4. The SAC shall charter a club or association, collectively known as an organization, under the following stipulations:
   4.a. Membership is open to any member of the Student Body;
   4.b. If the club is new, it cannot duplicate the services of an existing club or association;
   4.c. The organization has a Staff Advisor; and
   4.d. The organization shall not act contrary to the policies and guidelines laid out by the Toronto District School Board.
   4.e. Note that a club must complete an annual charter to renew its status.
   4.f. In May, the incoming Executive shall approve the continuation of each association's activities for the following school year. For early approval, each association must:
   I. Submit a club charter no later the May deadline chosen by the Executive. II. Have selected and documented the executive who will head the association for the following year.
   III. Have appointed a staff advisor who will be returning the following year.
   In the event that the staff advisor is unable to maintain their commitment to mentor the association come the new school year, the association must put forward another club charter with the updated information.
   4.g.
5. An organization must be chartered by the SAC in order to receive financial assistance from the SAC.
6. A Charter shall consist of the following details:
   6.a. The name of the organization
   6.b. The name of the President and other Executive members of the organization; 6.c. The name of the Staff Advisor,
   6.d. The purpose of the organization,
   6.e. The names of all participating members as of the date of submission. 6.f. Where the organization meets,
   6.g. When the organization meets, and,
   6.h. The contact information of the organization’s executive.
7. A Club Charter must be ratified by the Senate, and subsequently, the House of Representatives.​ The House of Representatives may revoke the charter at any time if the operations of the club are found to be detrimental to the welfare of the Student Body.
   7.a. Such claims must be substantiated and brought to the Executive. The club President and Executive members must be notified of their charter’s revocation a week prior to the next Senate meeting where they will be given the opportunity to justify their operations to the Senate, prior to the Senate voting on the revocation of the Club’s Charter.
8. All Club Charters must be approved in final by the SAC Staff Advisor(s)
9. Each organization member must have paid the Student Activity Fee, unless waived by their Vice Principal. See the Budget Act for further details regarding exceptions to the Student Activity Fee.
10. No Staff Advisor may advise for more than three SAC sanctioned organizations.
11. All posters, activities and events by the organization must be approved by the Club Staff Advisor, and open to removal, and/or change by the SAC Staff Advisor(s) and the Administration.
12. Refer to the Budget Act for further details concerning organizations sanctioned by the SAC.
    
## Chapter Five: Monies

### Article XVI: Grants
14. All SAC-sanctioned organizations within the school may make a formal request to the SAC if they require funds.
15. Requests for funds must include the Club/Association Charter as well as the following details:
    15.a. Amount requested,
    15.b. Intended use of funds,
    15.c. A list of other financial resources, and
    15.d. Signatures of the organization's President and Staff Advisor
16. All application for funds must be approved by the Executive and ratified by the Senate and House of Representatives.
17. SAC-sanctioned organizations which are found to have received funds through fraud or misrepresentation shall have their charter revoked and unspent funds seized. The charter may be restored the following year; however the organization or club shall be strictly audited for two years following the infraction.
    
### Article XVII: Finances
18. Finances of all SAC accounts shall be conducted under the provisions of the Budget Act.

## Chapter Six: Changes of Office
    
### Article XVIII: Impeachments
19. Any member of the House of Representatives or Senate may be impeached if he/she has acted contrary to an article or section of the Constitution or other effectual legislation applicable to their position or if he/she has acted to the detriment of the SAC and/or the Student Body.
20. The Senate is the sole body that can initiate a Bill of Impeachment naming a member of the Executive. Class Representatives have the right to petition the Senate to initiate such a Bill.
21. The Executive is the sole body that can initiate a Bill of Impeachment naming a member of the Senate or House of Representatives. The Grade Senators have the right to petition the Executive to initiate a Bill of Impeachment naming a Class Representative.
22. All Bills of Impeachment shall include a citation of the specific infraction with which the named party is accused, including the particular article and section of the
    Constitution. An impeachment form must be completed and present for a Bill of Impeachment to proceed.
23. Any person named in a Bill of Impeachment shall be notified of this a minimum of two weeks prior to voting in the Senate, and shall be given the opportunity to participate in a debate on the impeachment date.
24. If the person to who is the target of the Bill of Impeachment wishes to resign, he/she must submit the formal request in the form of a signed letter to the President. Once resigned, their case will be under investigation by either the Elections Committee or the Executive in the event that the resigned candidate wishes to run for/be appointed as a member of the Executive, Senate, or House of Representatives in the future.
25. A three-fourths majority (seventy-five percent) is required to pass a Bill of Impeachment. Both the Senate and House of Representatives must vote on the Bill of Impeachment with a closed ballot.
26. A class reserves the right to impeach its Class Representative/Vice-Representative by a sixty percent majority, closed ballot vote. Any member of a class may initiate such a procedure after informing the Vice-President. The Vice-President shall preside the voting
27. Any person successfully impeached by both the House and Senate will: 27.a. NOT be eligible to run for any Executive position for the following school year;
    27.b. NOT be allowed to sit on the Senate as a representative of any organization, club, event, or Grade the following school year;
    27.c. NOT be eligible for appointment of any other Senate position in the following school year; and
    27.d. NOT be eligible to run for Class Representative or Vice-Representative the following school year.
28. The Staff Advisor can inform a member of the Senate or House of Representatives if an Executive member has violated a part of the Constitution, or other effectual legislation.
    
### Article XIX: Speaker and Deputy Speaker Review
1. Either member of the Speaker position, that is, the Speaker or Deputy Speaker, may have their appointment reviewed if their actions are deemed to be contrary to the aims of their position and/or the general aims of the SAC.
2. The Executive shall be the sole body capable of initiating a review of the Speaker/Deputy Speaker; however, the House or Senate can petition the Executive for a Speaker/Deputy Speaker review.
3. The Executive shall explain the reason for the review to the House of Representatives. The Speaker/ Deputy Speaker may make one statement in their own defense. All other debates shall exclude the
   names and named parties, unless they are required to answer a question from the House of Representatives.
4. Speaker/Deputy Speaker review shall consist of two consecutive House of Representatives meetings, the first of which shall be to establish whether or not the review is necessary. The vote is to be a closed ballot and a three-fourths majority (seventy-five percent) must be reached for proceedings to continue, at which point the Speaker/Deputy Speaker must be warned that he/she will be reviewed by the House of Representatives.​ The second meeting will include the review debate at the end, during which a second closed ballot vote will be taken in order to establish whether or not the Speaker/Deputy Speaker will maintain office. A vote in favor necessitates a three fourth (seventy-five percent) majority, at which time the named Speaker/Deputy Speaker shall be removed from office.
5. Failure to achieve the required three-fourths (seventy-five percent) majority in the first vote will automatically cancel the second vote, and prohibit another review for at least two subsequent House of Representatives meetings.
   Article XX: Resignations
1. Resignations of any position shall be given to the President in a formal signed letter.
2. A Class Representative who fails to attend two House of Representatives meetings without sending the Vice-Representative as a replacement or without notifying the Executive or Speaker prior to the meeting of their absence, shall resign their position. Failure to resign shall result in an automatic initiation of impeachment proceedings.
   Article XXI: Vacancies of Office
1. If one or both of the Speaker and Deputy Speaker positions become vacant, a new Speaker shall be appointed by the Executive in consultation with the Staff Advisor and ratified by the House of Representatives and Senate.
2. If an Executive position becomes vacant – except for that of the President, which is filled by the VicePresident – candidates are nominated by the Student Body (requiring twenty-five nominations, as per the
   Elections Act) from the House of Representatives or the Student Body, and voted on by the House of Representatives after short speeches are given. Candidate criteria as outlined in Article IV of the Election Act is applicable.
3. If a position in the Senate becomes vacant, a replacement may be appointed by the Executive and must be ratified by a simple fifty-one percent majority in the House of Representatives and Senate.
4. If a Grade Senator position becomes vacant, the members of the House of Representatives of the particular grade must elect a new Senator using the Procedures of Senatorial Election as outlined in the Elections Act regarding the election of the Grade Nine Senator. The Executive will reassign other Senate positions.
5. If a position in the House of Representatives become open, the Vice-Representative will assume their office, as per the Elections Act.
6. If a vacancy results as from a non-confidence vote against the entire Executive body, Section 2 of this Article is followed.
   
### Article XXII: Elections
7. Elections of all SAC positions shall be conducted under the provisions of the Elections Act.
   
## Chapter Seven: Limitations and Enforcement

### Article XXIII: Constitutional Amendments
8. A Bill of Constitutional Amendments may be presented by any member of the House of Representatives to the Speaker. Amendments to the Constitution shall only be made once every three sitting SACs, unless the Staff Advisor and the Speaker have given their permission in a semi-emergency situation.
9. A Bill proposing major amendments shall be presented to a Constitutional Reform Committee at the end of the school year.
10. An amendment to the Constitution shall require a three-fourths majority (seventy-five percent) in the House of Representatives.
11. Any amendment which alter the essential meaning of an article, section, or subsection of the Constitution shall be implemented only for the following year, and shall be added to the current Constitution as an amendment.
12. A copy of amendments to the Constitution shall be printed when necessary, and made available to all members of the House of Representatives.
    Article XXIV: Conflict of Interest
13. All members of the SAC shall act only in the best interest of the SAC and the Student Body.
14. No member of the SAC shall conduct any business or organize any event of direct conflict against the aims of the SAC, or which infringe upon SAC jurisdictions.
15. No member of the SAC shall in any manner, abuse their position nor use the position for personal profit, or to the detriment of the SAC or the Student Body.
16. Any member of the SAC who acts contrary to their position, for personal profit, or to the detriment of the Student Body shall be subject to a review with grounds for impeachment by the appropriate body.
17. No member of the SAC may represent or claim to represent the SAC without prior approval from the SAC Executive.
18. SAC Executive members perform leadership roles within the school. They should conduct themselves as such leaders. Members will be open for removal from their Executive positions if their behaviour is deemed to be detrimental to their status and representation amongst the Staffing Group and the Student Body.
    Article XXV: Referendums, Non-Confidence, and Petitions
19. A referendum may override a SAC decision or decide a particularly controversial issues.
20. If a petition containing the signatures of twenty-five percent of the Student Body is submitted concerning a certain issue, a vote by the entire Student Body is required. In
    order for the petition to result in changes to SAC policies, no less than seventy percent of the Student Body must vote; and of that, a simple majority (fifty percent plus one) must be obtained.
21. All petitions to gain status as an Association, or to gain Senatorial representation must gain two thirds support from the Executive (sixty-seven percent) and seventy-five percent support in the Senate and House of Representatives, respectively.
22. If there is an issue in which a Senator or Class Representative feels that a certain issue has not been sufficiently addressed, he/she can petition the Chair of the Senate or Speaker with a formal bill signed by all members of their group in order for the issue to be considered or reconsidered.
23. Petitions for the Executive to consider or reconsider a certain issue must contain at least sixty percent of the House of Representatives membership.
24. A motion of non-confidence can be presented by any House or Senate member against the entire Executive body. The motion must contain specific and concrete references to the reasons for nonconfidence. A vote can be brought forward at a House of Representatives meeting. A vote of nonconfidence must have unanimous (one hundred percent) approval of the House of Representatives for it to pass. If the motion passes, the member(s) in question must resign their/their position of government, and Article XXI: Vacancies of Office will be followed.
25. If a petition containing the signatures of ten percent of the student body is submitted concerning a certain issue, the Senate and Executive must consider or reconsider the topic.
    
## Appendixes

### Appendix A

Please see following attachments, all of which are effectual legislation:
- NSS Student Council Charter
- Public Voting Record Bill – approved Nov 30, 2005
- Ease of Identification Act
- Assembly Use Restriction Act
- An act to create a Student Council Records Library
- Closed Ballot form – Revised 2009
- Robert's Rules of Order
- Senate Procedures and Terms
- Impeachment Form
- Excusal Form
- Rules of Order for House of Representatives

## Appendix B

Principle Authors:
- Constitutional Reform Committee, 1987
- Greg Beilles (Chair), Dale Callender, Rob Catto, Tamara Coarse, Kimberly Heffernan, Alan Kriss,
- Noah Novogrodsky, Lowell Pancer, Ali Velshi, Elizabeth Stock

Revised and Amended by:
- Student Council Improvement Committee, May 1991
- Ari Berger, Jason Charters, Jason Hughes. Lisa Stevenson

Amendments by:
- Elections Committee, 1995
- Gina Poulos (SAC Advisor), Giselle Smejda (Chair), Margaret Feiner, Michael Mills, Craig Peskin, Jamie
- Silverberg, David Treleaven

Revised and Amended by:
- The TM Podium Avengers, 2000
- James S. Fraser (Vice-Chair Election Committee 2000, Principal Author: Elections Act – 2000), Debra Hamer (Chair: Elections Committee 2000), Susan Honess (Lamp: Elections Committee 2000), Ethan Siller (Chair: Elections Committee 1999)

Revised and Amended by:
- NSS CRC 2002
- Jason Freedman (Chair, President Northern Student Council), Max B. Rubin (Deputy Chair, TDSB Super Council Senator), Jonathan Goldsbie (Deputy Chair, Epigram Senator), Margot Blankier (Yearbook Senator), Dan Goudge (Internal Social Director Northern Student Council), Dan Revich (Deputy Speaker), Jacob Fox (Grade Nine Senator), Dale R. Callender – staff advisor, Tony Carella – staff advisor

Revised and Amended in 2004 by:
- Gavin Nowlan- President Student Council, Dale R. Callender – Staff advisor, Max Rubin - consultant

Revised and Amended in 2005 by:
- Northern Student Council Executive 2005, Dale R. Callender – Student Council Staff Advisor, Sudeshna Dutta –
- Student Council Staff Advisor

Revised and Amended in 2008 by:
- Daniel Rodic – S.A.C Vice-President, 2007 – 2008, Dale R. Callender – Student Council Staff Advisor

Revised and Amended in 2009 by:
- Q Carol Xiong (SAC Secretary, 2008-09), Ms Lori Moulton (Staff Advisor, 2008-09)

Revised and Amended in 2011 by:
- Oren Lefkowitz (SAC President, 2010-11), Andrew Wong (SAC Speaker, 2010-11),
- Dale R. Callender (Staff Advisor, 2010-2011)

Revised and Amended in 2017 by:
- Carolyn McLeod (SAC President, 2016-17), Arjun Jagota (SAC Treasurer 2015-17), Connor White, Becky Milner, John Riccardi, Jacob Landau
- Dale R. Callender (Staff Advisor)