+++
title = "About"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata"]
+++

Welcome to the Northern SAC / NSS SAC website! This is where you can find information about the SAC Execs, events, 
club applications and documents.

If you have a new club and want to be featured on the clubs page fill out this form: [Clubs and Associations](/clubs)
If you have any events that you would like on the calendar please [contact us](/contact).

## Webmasters

2020-2021: [Ken Shibata](https://gitlab.com/colourdelete)

2019-2020: Brandon Fowler

2018-2019: Jori Reiken

2017-2018: Jonathan Katz

## People

{{< objs id="persons" name="People" url="http://gitlab.com/nsssac/data/-/raw/master/persons/persons.json" noGovernance=true noMtgDetails=true noSocials=true >}}
