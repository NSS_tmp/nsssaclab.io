+++
title = "COVID-19 Protocols & Updates"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata", "Aleksi Toiviainen"]
+++

# For the case count of COVID-19 cases in Northern Secondary School, please visit:

[COVID-19 Protocols and Updates on the northernsac.ca website](https://www.northernsac.ca/covid-19-protocols-and-updates)

# Updated COVID-19 Screening for Symptoms

Before coming to school, all staff and students must conduct a self-assessment for COVID-19 symptoms. This assessment can be done though the Health Screening App, called Now Mobile, from the Google Play Store and the Apple App Store. Students can also print the TDSB Health Screening Paper Pass, fill out the questionnaire, sign it and bring it to school.

If the student has one or more of the symptoms, even if it is mild, the student should follow the protocols outlined below. 

*Symptoms include:*

- Fever over 37.8°C
- Cough
- Difficulty breathing
- Loss of taste or smell
- Sore throat/painful swallowing
- Stuffy/runny nose
- Headache
- Nausea/vomiting/diarrhea
- Feeling unwell/muscle aches/tiredness. 

Students with medically diagnosed chronic health issues that are unrelated to COVID-19 should look for new, different or worsening symptoms.

- If a student has one or more of the listed symptoms (even if mild), they should stay home, self-isolate, get tested or contact their health care provider.
- If a student has been in close contact with a person who has COVID-19, they will need to self-isolate for 14 days, even if they don’t have symptoms. If the student has symptoms, their household members will need to stay home and self-isolate until COVID-19 is ruled out.
- If a student has travelled outside of Canada, they must self-isolate for 14 days. Their household members do not need to self-isolate if they have not travelled, have no symptoms of COVID-19, and have not been in close contact with someone who has COVID-19.

## Siblings or Children in Same Household Without Symptoms 

- If a student has symptoms and is diagnosed with something unrelated to COVID-19 (a cold or respiratory infection is not considered an alternative diagnosis and the student should get tested for COVID-19), siblings do not need to self-isolate and can return to school.
- If a student has symptoms and tests negative for COVID-19, the student and their siblings do not need to self-isolate and can return to school.
- If a student has symptoms, DOES NOT get tested and HAS NOT been in close contact with someone who has COVID-19, the student and their siblings must self-isolate for 10 days.
- If a student has symptoms, DOES NOT go for testing and HAS been in close contact with someone who has COVID-19, the student must self-isolate for 10 days from when the symptoms started and siblings should self-isolate for 14 days
- If a student has symptoms and tests positive, the student must self-isolate for 10 days and siblings must self-isolate for 14 days.

Students may return to school after 10 days if they do not have a fever (without the use of medication) and if their symptoms have been improving for at least 24 hours. Students without symptoms still have to stay home and self-isolate for 10 days, from the day of the received the test results. All members in the same household as the student should stay home, self-isolate and follow public health advice. 

Adults living in the same home as the student (including parents) will have to self-monitor for symptoms and may go to work as long as the adult does not have symptoms, the student is not a close contact of someone with COVID-19 and the student has not tested positive.

## Students Who Test Negative for COVID-19

Students can return to school if their symptoms have been improving for at least 24 hours, and if they have not been in close contact with someone with COVID-19 or have not travelled outside of Canada.

## Students with an Alternative Medical Diagnosis Who are Not Tested For COVID-19

Students with symptoms, but who have been given an alternative diagnosis (not related to COVID-19) by a health care provider may return to school if their symptoms have been improving for at least 24 hours. Family members without symptoms should self-monitor and may go to school or work. A cold or respiratory infection is not considered an alternative diagnosis and the student should get tested for COVID-19.

## Students Not Tested for COVID-19

If a student has symptoms of COVID-19 that are unrelated to an existing medical condition and does not get tested, the student and their siblings must stay home and self-isolate for 10 days from the day their symptom(s) first started. The student can return to school after 10 days if they do not have a fever (without the use of medication), and their symptoms have been improving for 24 hours.

## Changes Made to Northern to Prevent the Spread of COVID-19

- Students and staff practice physical distancing to the maximum extent possible. 
- Classrooms are organized to encourage the maximum space between students. 
- Floors are marked with stickers to designate a one-way traffic flow and to identify 2 metre distances. 
- Signs are placed throughout the school to reinforce safety protocols. 
- To avoid large gatherings, the maximum number of people in common areas such as hallways, stairwells and libraries is reduced 
- Lockers are not being used by students
- The cafeteria is closed and food services are not being provided.
- Students and staff must disinfect hands with alcohol-based sanitizer before entering the school
- Alcohol-based sanitizers are available throughout the school and at the designated entry and exit points
- All staff members have been provided with the appropriate PPE (medical masks and face shields) to safely interact with each other and students
- All visitors and staff entering the school are recorded for contact tracing.
- Students are required to only enter the school door that corresponds to the floor on which their class for the day is located.

![School Entrances Per Floors](https://lh6.googleusercontent.com/fVMZnezopOTJ71lg5uQBQuMK00tmXGCJOaMMhRN5wH17F1ykXcv58oaBj2y5hW6mazmWwqLi0ApEeMZ8ybUd7jtn3cRdYuJ6N69erDSQF7Zj8iDiIDihIiDjceloRtyf7g "School Entrances Per Floors - Students on floor 4 should enter through the entrance for students on floor 3. If the teacher says otherwise, please follow the teacher's guidance.")

### School Visitors 

*Northern has significantly limited visitors, including parents/guardians.* When possible, visitors are requested to conduct school business via phone/email. All visitors must make an appointment with the VP office before entering the building. 

To make an appointment, call the VP office at 416-393-0284 (ext. 20003 for students with last names starting with A-G, ext. 20008 for students with last names starting with H-O and ext. 20007 for students with last names starting with P-Z).

*All visitors must*:

- Conduct a COVID-19 health screening at the main school entrance
- Wear a medical mask while on school property. 
- Sign in at the office of the Vice President (VP Office).
