+++
title = "Contact Us"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata", "bot"]
+++

## Contact the SAC

The preferred contact method is {{< instagram "sacnss" >}}, but you can use {{< email "webmaster.nsssac@gmail.com" >}} 
too. Please do not contact us through other methods such as personal emails or accounts of the members of the SAC.

___

For issues about this website in particular, you can file an issue that will be published publicly 
[via email](mailto:incoming+nsssac-nsssac-gitlab-io-23247167-cqmduuzelpnx4yysq0c0bjvbh-issue@incoming.gitlab.com) or
[via GitLab.com](https://gitlab.com/nsssac/nsssac.gitlab.io/-/issues).

{{% scream %}}
{{< icon "mdi-alert" >}}
This information will be published publicly at [GitLab.com](https://gitlab.com/nsssac/nsssac.gitlab.io/-/issues).
{{% /scream %}}

## Contact a specific member of the SAC

{{< objs id="persons" name="People" url="http://gitlab.com/nsssac/data/-/raw/master/persons/persons.json" noGovernance=true noMtgDetails=true noSocials=true >}}
