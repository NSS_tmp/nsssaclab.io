+++
title = "School Council"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata"]
+++

[Visit the School Council Site](https://nsscouncil.com)

The school council is a group of parents, volunteers, and representatives whose goal is to improve the quality of life at Northern. Through meetings with the principal and representatives from the student council, the school council seeks to hear from a wide range of voices about current problems and events at Northern, and what can be done to fix them. The council has its own budget, separate to that of Northern's Student Affairs Council treasury which is used to fund activities, clubs and repairs around the school.

For questions or to voice your thoughts to the school council, contact PTA Senator Gethin James.
