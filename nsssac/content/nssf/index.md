+++
title = "Northern Secondary School Foundation"
date = "1970-01-01T00:00:00"
draft = false
tags = ["NSSF"]
authors = ["Ken Shibata"]
+++

[Visit the NSSF Site](https://nssf.ca)

> To support Northern’s students and teachers, to empower them through enhanced learning and teaching experiences, and to recognize them through the administration of scholarships, awarding of grants, and facilitation of special projects.

The NSSF is a charitable organization dedicated to Northern Secondary School. Students and student organisations have relied on funding from the NSSF for years to pursue their goals.
