+++
title = "よくある質問"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata"]
+++

生徒委員会や学校についてよくある質問などをまとめました。

{{< objs id="faqs" name="FAQs" url="http://gitlab.com/nsssac/data/-/raw/master/faqs.json" details=true noType=true noActions=true noImage=true noTime=true noNotice=true noMtgDetails=true noSocials=true noGovernance=true >}}
