+++
title = "Schools Closed to In-Person Learning from 2021-04-07"
date = "2021-04-06"
draft = false
tags = ["COVID-19 / SARS-CoV-2"]
authors = ["Ken Shibata"]
+++

TDSB schools will be closed to in-person learning from 2021-04-07 or 2021-04-06 [^3] for at least 2 weeks [^1] [^2].

According to the Globe and Mail, "[an] announcement [about this situation] is expected Tuesday afternoon".

Thank you to [the SAC Vice President (as of 2021-04-06)](/about/#persons-obj-sac-exec-vp) for reporting this to [the Webmaster](/about/#persons-obj-sac-senator-webmaster-2020).

An email [^4] from the TDSB was sent to parents about this.

[^1]: https://www.theglobeandmail.com/canada/article-toronto-schools-to-close-in-person-classrooms-on-wednesday-shift-to/?fbclid=IwAR1GJVorPSNQaFMjVIFdG8wNiEbPBrCNw4oLGICcZ9Qp1htfigxGH7Baj0o

[^2]: https://www.thestar.com/politics/provincial/2021/04/06/toronto-schools-to-shut-down-wednesday-as-covid-cases-rise.html?fbclid=IwAR1ty2y4oA7nsK8LAwjrs2mE0fxRebpRK74CcWTLT8r8zdHyuC8RsoIfsHk

[^3]: The title of [^1] says Wednesday, but the 4th paragraph says Tuesday.

[^4]: [Email](school-closure)
