+++
title = "Email: Important Update Re: School Closures"
date = "2021-04-06T14:34:00-05:00"
draft = false
tags = ["COVID-19 / SARS-CoV-2", "Source"]
authors = ["Ken Shibata"]
+++

From: Toronto District School Board <noreply@tdsb.on.ca>
Date: Tue, Apr 6, 2021 at 2:34 PM
Subject: Important Update Re: School Closures
To: `redacted`

Dear Parents/Guardians and Students,

This is a very important update about the status of in-person learning at the TDSB. Toronto Public Health (TPH), has just announced that all schools in Toronto, including those at the TDSB, will be closed to in-person learning as of Wednesday, April 7, 2021 and remain closed up to and including Sunday, April 18, 2021. This TPH order is being made under Section 22 of Ontario’s Health Protection and Promotion Act in an effort to slow the spread of COVID-19.

For the remainder of this week, students and staff will move to remote learning and the April Break scheduled for next week will continue as normal. Please note that, unlike the closure in January and February, this applies to all TDSB students in all TDSB schools. School-aged children will not be permitted to attend daycares located inside TDSB schools during the closure, however, pre-school-aged children will continue to be permitted to attend.

While we recognize that you may still have a number of questions, please know that we will share additional information once we receive it. In the meantime, we wanted to ensure you received this initial information as soon as possible. We know that this TPH order will impact our families in a number of different ways but we will do all that we can to support students and staff through this period. Teachers will be connecting with families about next steps on remote learning in the days ahead.

Thank you.

___

Toronto District School Board would like to continue connecting with you via email. If you prefer to be removed from our list, please contact Toronto District School Board directly. To stop receiving all email messages distributed through our SchoolMessenger service, follow this link and confirm: Unsubscribe

SchoolMessenger is a notification service used by the nation's leading school systems to connect with parents, students and staff through voice and email.
