+++
title = "Turn on 2FA to Protect your Account(s)"
date = "2021-02-23T16:25:12-05:00"
draft = false
tags = ["Security"]
authors = ["Ken Shibata"]
+++

Turn on 2FA. It may help protect account(s) from hackers. It may not, but it’s better safe than sorry. Having 2FA will only have minor inconveniences, as with many security measures.

Benefits: 2FA may protect your account even if your password is compromised.

Inconveniences: you may need a phone to sign in and it may take a few more seconds to sign in.
What is 2FA, Why Use It and How Does it Protect my Account(s)?

2FA (Two Factor Authentication) is a way to protect your account(s) from unauthorized access. They are many ways to verify a sign in attempt that are called 2FA. The most common style of 2FA is the service provider (e.g. Google) sends a text message to your phone with a unique number that is required for you to sign in. You still need your username and password.
This is a diagram of the sign in steps on a given platform with 2FA enabled and disabled. The flow on the left is with 2FA enabled. It adds an extra layer of security, and thus protect account(s) more.
The flow on the left is with 2FA enabled. It adds an extra layer of security.

![The flow on the left is with 2FA enabled. It adds an extra layer of security.](fix)

*The flow on the left is with 2FA enabled. It adds an extra layer of security.*

Another style of 2FA is through a “verification code generator app”, which essentially does the same thing but without sending a text message. An example of this type of app is Google Authenticator (available on Android and iOS). This may be useful if you don’t want text messages to be sent, or if you don’t want to use a phone.
