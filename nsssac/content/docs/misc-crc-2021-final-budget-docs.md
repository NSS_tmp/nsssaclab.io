+++
title = "Final Recommendations of the CRC for the Budget Act (Document Format)"
date = "2021-05-08"
draft = false
tags = ["miscellaneous doc", "object", "doc" , "crc" , "approved senate" , "approved house" , "no-show"]
authors = ["bot"]
presidents = [""]
execs = [""]
staff = [""]
+++

{{< obj id="misc" sub_id="crc-2021-final-budget-docs" url="http://gitlab.com/nsssac/data/-/raw/master/misc/misc.json" noGovernance=true >}}
