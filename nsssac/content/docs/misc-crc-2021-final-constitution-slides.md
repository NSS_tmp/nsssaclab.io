+++
title = "Final Recommendations of the CRC for the Constitution (Slideshow Format)"
date = "2021-05-15"
draft = false
tags = ["miscellaneous doc", "object", "doc" , "crc" , "approved senate" , "no-show"]
authors = ["bot"]
presidents = [""]
execs = [""]
staff = [""]
+++

{{< obj id="misc" sub_id="crc-2021-final-constitution-slides" url="http://gitlab.com/nsssac/data/-/raw/master/misc/misc.json" noGovernance=true >}}
