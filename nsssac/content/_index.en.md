+++
title = "Northern SAC"
date = "1970-01-01T00:00:00"
draft = false

authors = ["Ken Shibata"]
tags = ["Home"]
+++

{{< days "https://gitlab.com/nsssac/data/-/raw/master/days.json" >}}

The {{< abbr "NSS SAC" "Northern Secondary School Student Affairs Council" >}} is the student government of
{{< abbr "NSS" "Northern Secondary School" >}}.

On this website you can find info on upcoming SAC events, [the SAC executive]({{< relref "about" >}}),
[clubs]({{< relref "clubs" >}}), and more!
