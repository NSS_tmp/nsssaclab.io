+++
title = "SAC Exec and Grade Senator Elections for 2021-2022"
date = "2021-05-28"
draft = false
tags = ["elections", "elections for 2021-2022", "2020-2021"]
authors = ["Ken Shibata", "Aleksi Toiviainen"]
+++

## [Candiates & Positions](/elections/for-2021-2022/candidates)

## Voting Instructions

<div class="w3-half">
<b><h2>Voting Started <br> 09:00 on<br>Tuesday 2021-06-01</h2></b>
</div>
<div class="w3-half">
<b><h2>Voting Ends <br> 15:15 on<br>Friday 2021-06-04</h2></b>
</div>

{{< icon "mdi-alert" >}} Some students may get the ballot email after 09:00 on Tuesday 2021-06-01. According to Aleksi Toiviainen, the SAC VP of the 2020-2021 school year, everyone should have received their ballot email by 15:36:21 on Tuesday 2021-06-01.

{{< icon "mdi-alert" >}} Voting rescheduled to make sure students have an informed vote by watching the campaign videos.

The Elections Committee is planning to give students class time to watch all campaign videos *before the election timeframe* (time undecided).

1. You will receive a ballot *via email*.
2. Vote using the ballot by Friday 2021-06-04 to vote.

## Eligibility

Any student currently enrolled at NSS can vote.

The *SAC Exec and Grade Senator Elections for 2021-2022* is the elections for
SAC Executives and Grade Senators for the 2021-2022 school year.
