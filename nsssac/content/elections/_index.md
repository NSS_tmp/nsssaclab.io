+++
title = "Elections"
date = "1970-01-01T00:00:00"
draft = false
authors = ["Ken Shibata", "Aleksi Toiviainen"]
+++

## You may be looking for the [SAC Exec and Grade Senator Elections for 2021-2022](/elections/for-2021-2022).

## Elections

On this page you will find everything you need to know about the upcoming SAC Elections.

If you want to know anything about any of the positions feel free to [contact us](/contact) anytime!

### Class/Cohort Rep Elections

The SAC has three parts: the Executive, Senate, and House or Representatives (House of Reps).

The House of Reps is how all the students of Northern can make sure their voices are heard and acted upon.
The Cohort Representatives report the issues of their class or cohort to the House of Reps.
The House of Reps will usually be meeting each month lead by the Speaker and the Executive.
For more information and details please see the Google Doc below.

[Cohort Rep Elections: Explanatory Document – Google Docs](https://docs.google.com/document/d/1llPiBNDERAi2ds3yDsXCRp_BAEeiLAChiEiV7fePbcY/edit)

[Elections Act (PDF)](https://gitlab.com/nsssac/data/-/raw/master/elections-act.pdf)

If you want to know anything about any of the positions feel free to contact the SAC via email or Instagram.
Also, you can message the Elections committee during elections season!
