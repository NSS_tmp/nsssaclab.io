+++
title = "Constitutional Reform Committee"
date = "1970-01-01T00:00:00"
draft = false
+++

[Constitutional Reform Committee Documents]({{< relref "/tags/crc" >}}) •
[Budgets]({{< relref "/tags/budgets" >}}) •
[Minutes]({{< relref "/tags/minutes" >}}) •
[Miscellaneous Documents]({{< relref "/tags/miscellaneous-doc" >}})

The Constitutional Reform Committee is a body tasked with reviewing and maybe changing the Constitution of the Northern SAC.
The Constitution controls how the clubs, associations, this SAC and the budget operates.

[View the current Constitution]({{< relref "/docs/misc-constitution" >}})

[View Proposed Changes]({{< relref "/tags/crc" >}})

## Applications (closed)

Applications have closed on February 9th 2021.
