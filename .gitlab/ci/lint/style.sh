#!/bin/sh

find . -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\|scss\)$' -exec stylelint {} \;
