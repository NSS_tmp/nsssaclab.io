#!/bin/sh

mkdir -p ./mirror-tmp
cd ./mirror-tmp
git clone --bare https://gitlab.com/nsssac/nsssac.gitlab.io.git ./old
cd ./old
git lfs fetch --all || echo "Git LFS fetch failed."
git push --mirror $GITHUB_MIRROR_PUSH_URL
git lfs push --mirror $GITHUB_MIRROR_PUSH_URL || echo "Git LFS failed."
cd ../..
rm -r ./mirror-tmp
