#!/bin/sh

mkdir ./log
mkdir ./src
mkdir ./bin
pacman -Syu git go nodejs npm gcc gzip --noconfirm > log/apt-get-install.log
git --version
go version
node --version
npm --version
git clone https://github.com/gohugoio/hugo.git ./src/hugo
export GOBIN=$PWD/bin
cd ./src/hugo
git checkout $(git describe --tags $(git rev-list --tags --max-count=1))
go install --tags extended > ../../log/go-install.log
go build -o=../../bin/hugo --tags extended > ../../log/go-build.log
chmod +x ../../bin/hugo
cd ../..
export PATH=$PWD/bin:$GOPATH/bin:$PATH
npm i
npm i postcss-cli
npm i -g postcss-cli
