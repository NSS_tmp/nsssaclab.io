#!/bin/sh
apt -q update
apt -yq upgrade
apt -yq install git
echo "Install Git LFS"
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash || echo "Git LFS install (1/2) failed."
git lfs install || echo "Git LFS install (2/2) failed."
